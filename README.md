# What is JIRA?

JIRA is a proprietary issue tracking product, developed by Atlassian. It provides bug tracking, issue tracking, and project management functions. Although normally styled JIRA, the product name is not an acronym, but a truncation of Gojira, the Japanese name for Godzilla. It has been developed since 2002.

> [wikipedia.org/wiki/JIRA](https://en.wikipedia.org/wiki/JIRA)

![logo](http://upload.wikimedia.org/wikipedia/en/thumb/b/bf/JIRA_logo.svg/125px-JIRA_logo.svg.png)

## Quick Install

Enter the following to get JIRA up and started quickly:

```bash
/usr/bin/docker run -it --rm --publish 8080:8080 raynera/jira
```