FROM raynera/java
MAINTAINER Andrew Rayner <a.d.rayner@gmail.com>

ENV JIRA_VERSION 6.4.3
ENV JIRA_HOME /var/jira
ENV JIRA_INSTALL /opt/jira
ENV JIRA_DOWNLOAD http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-${JIRA_VERSION}.tar.gz

RUN mkdir -p ${JIRA_INSTALL} \
	&& mkdir -p ${JIRA_HOME}  \
    && curl -Lks ${JIRA_DOWNLOAD} | tar -xzv --strip-components=1 -C ${JIRA_INSTALL}
     
COPY entrypoint.sh /entrypoint.sh

RUN useradd -r -u 1000 -s /sbin/nologin atlassian \
	&& chown -R atlassian ${JIRA_INSTALL} \
	&& chmod +x /entrypoint.sh

VOLUME ["${JIRA_HOME}", "${JIRA_INSTALL}/logs", "${JIRA_INSTALL}/temp", "${JIRA_INSTALL}/work"]

EXPOSE 8080

ENTRYPOINT ["/entrypoint.sh"]
CMD ["run"]
