#!/bin/bash

set -e

# Increase plugin wait time so JIRA has enough time to load all the plugins on slower machines (such as AWS shared instances)
sed -i "s:JVM_SUPPORT_RECOMMENDED_ARGS=\"\":JVM_SUPPORT_RECOMMENDED_ARGS=\"-Datlassian.plugins.enable.wait=300\":" ${JIRA_INSTALL}/bin/setenv.sh

# Apply proxy settings if provided
if [ ! -z "${PROXY_URL}" ] ; then
	sed -i "s:debug=\"0\" URIEncoding=\"UTF-8\":debug=\"0\" URIEncoding=\"UTF-8\" proxyName=\"${PROXY_URL}\" proxyPort=\"${PROXY_PORT:-80}\":" ${JIRA_INSTALL}/conf/server.xml
fi

# Disable websudo
if [ ! -f ${JIRA_HOME}/jira-config.properties ]; then
    echo "jira.websudo.is.disabled = true" > ${JIRA_HOME}/jira-config.properties
fi

# Execute JIRA
if [ "$1" = "run" ]; then
	chown -R atlassian ${JIRA_HOME} ${JIRA_INSTALL}/logs ${JIRA_INSTALL}/temp ${JIRA_INSTALL}/work
  	exec gosu atlassian ${JIRA_INSTALL}/bin/start-jira.sh run
fi

exec "$@"
